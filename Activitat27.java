import java.util.Scanner;

public class Activitat27 {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\033[0;32m";
    public static final String ANSI_PURPLE = "\u001B[35m";

    public static Scanner teclado;
    public static final int MAX_TASQUES = 20;
    public static int CONT = 0;

    public static void main(String[] args) {
        teclado = new Scanner(System.in);
        String[][] matriz = new String[MAX_TASQUES][20];
        empezar(matriz);
    }

    public static void empezar(String[][] matriz) {
        cabecera();
        int opcion = pideNumero();
        menu(opcion, matriz);
    }

    public static int pideNumero() {
        int opcion = 7;
        System.out.print("Introdueix l'opció[1-6]: ");
        if (teclado.hasNextInt()) {
            opcion = teclado.nextInt();
        } else {
            System.out.print(ANSI_RED + "Error! Has d'introduir un valor numéric\n" + ANSI_RESET);
        }
        return opcion;
    }

    public static void cabecera() {
        System.out.println();
        System.out.println("Benvigut a Batoi To-Do List\n" +
                "---------------------------\n");
        System.out.println("OPCIONS DISPONIBLES");
        System.out.println("1. Afegir una tasca\n" +
                "2. Mostrar tasques\n" +
                "3. Mostrar tasques ordenades per temps\n" +
                "4. Afegir temps\n" +
                "5. Marcar tasca com a finalitzada\n" +
                "6. Eixir del programa");
        System.out.println("\n\n");
    }

    public static void menu(int opcion, String[][] matriz) {

        switch (opcion) {
            case 1:
                afegixTasca(matriz);
                empezar(matriz);
                break;
            case 2:
                mostrarTasques(matriz);
                empezar(matriz);
                break;
            case 3:
                ordenarMatriz(matriz);
                empezar(matriz);
                break;
            case 4:
                afegirTemps(matriz);
                empezar(matriz);
                break;
            case 5:
                marcarFinalitzada(matriz);
                empezar(matriz);
                break;
            case 6:
                System.out.println(ANSI_PURPLE + "Adéu" + ANSI_RESET);
                System.exit(-1);
                break;
            default:
                System.out.println(ANSI_RED + "Error! Has de introduïr un valor entre 1 i 6" + ANSI_RESET);
                empezar(matriz);
        }
    }

    public static void mostrarTasques(String[][] matriz) {
        System.out.println();
        if (CONT == 0) {
            System.out.println(ANSI_RED + "No hi han tasques afegides" + ANSI_RESET);
            System.out.println();
            empezar(matriz);
        }
        for (int i = 0; i < CONT; i++) {
            System.out.println(ANSI_PURPLE + "Tasca " + (i + 1) + " => '" + matriz[i][1] + "' a realitzar en " + matriz[i][3] + " minuts [Prioritat " + matriz[i][4] + "] (" + matriz[i][5] + ")" + ANSI_RESET);
        }
    }

    public static void afegixTasca(String[][] matriz) {
        System.out.print("Descripció de la tasca: ");

        teclado.nextLine();
        String tasca = teclado.nextLine();

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] == null) {
                    matriz[CONT][1] = tasca;
                    matriz[CONT][5] = "Pendent";
                }
            }
        }

        System.out.print("Tipus[Hardware, Xarxa, Programació, Base de dades, Altre]: ");
        String tipus = teclado.next();

        if (tipus.equalsIgnoreCase("hardware") || tipus.equalsIgnoreCase("xarxa") || tipus.equalsIgnoreCase("programacio")
                || tipus.equalsIgnoreCase("base de dades") || tipus.equalsIgnoreCase("altre")) {
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (matriz[i][j] == null) {
                        matriz[CONT][2] = tipus;
                    }
                }
            }
        } else {
            System.out.println(ANSI_RED + "Error! El valor introduït no és vàlid\n" + ANSI_RESET);
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    matriz[CONT][j] = null;
                }
            }
            afegixTasca(matriz);
        }

        System.out.print("Temps estimat[5-1000]: ");
        int temps = teclado.nextInt();
        if (temps >= 5 && temps <= 1000) {
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (matriz[i][j] == null) {
                        matriz[CONT][3] = String.valueOf(temps);
                    }
                }
            }
        } else {
            System.out.println(ANSI_RED + "Error! Has de introduïr un valor entre 5 i 1000" + ANSI_RESET);
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    matriz[CONT][j] = null;
                }
            }
        }

        System.out.print("Prioritat[Baixa, Mijta, Alta]: ");
        String prioritat = teclado.next();
        if (!prioritat.equalsIgnoreCase("baixa") || !prioritat.equalsIgnoreCase("mitja") || !prioritat.equalsIgnoreCase("alta")) {
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    if (matriz[i][j] == null) {
                        matriz[CONT][4] = prioritat;
                    }
                }
            }
        } else {
            System.out.println(ANSI_RED + "Error! El valor introduït no és vàlid\n" + ANSI_RESET);
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    matriz[CONT][j] = null;
                }
            }
            empezar(matriz);
        }
        System.out.println(ANSI_GREEN + "S'ha afegit la tasca satisfactòriament. La tasca ocupa la posició " + (CONT + 1) + "\n" + ANSI_RESET);
        CONT++;
    }

    public static void marcarFinalitzada(String[][] matriz) {
        if (CONT == 0) {
            System.out.println(ANSI_RED + "No hi han tasques afegides" + ANSI_RESET);
            System.out.println();
            empezar(matriz);
        }

        System.out.println();
        System.out.print("Quina tasca vols finalitzar?");
        int tareaFinalizar = teclado.nextInt();

        if (matriz[tareaFinalizar - 1][5] == "Finalitzada") {
            System.out.println(ANSI_RED + "La tasca ja està finalitzada" + ANSI_RESET);
        } else if (matriz[tareaFinalizar - 1][5] == null) {
            System.out.println(ANSI_RED + "La tasca encara no s'ha creat" + ANSI_RESET);
        } else {
            System.out.println(ANSI_GREEN + "Tasca " + tareaFinalizar + " finalitzada satisfactòriament" + ANSI_RESET);
        }

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[tareaFinalizar - 1][5] = "Finalitzada";
            }
        }
    }

    public static void ordenarMatriz(String[][] matriz) {
        if (CONT == 0) {
            System.out.println(ANSI_RED + "No hi han tasques afegides" + ANSI_RESET);
            System.out.println();
            empezar(matriz);
        }

        String[][] aux = new String[MAX_TASQUES][20];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (matriz[i][j] != null) {
                    aux[i][j] = matriz[i][j];
                }
            }
        }

        String[] aux1 = new String[20];
        for (int i = 0; i <= aux.length; i++) {
            for (int j = i + 1; j < aux.length; j++) {
                if (aux[i][3] != null && aux[j][3] != null) {
                    if (Integer.parseInt(aux[i][3]) < Integer.parseInt(aux[j][3])) {
                        for (int k = 0; k < aux[i].length; k++) {
                            aux1[k] = aux[i][k];
                        }
                        for (int k = 0; k < aux[i].length; k++) {
                            aux[i][k] = aux[j][k];
                        }
                        for (int k = 0; k < aux[i].length; k++) {
                            aux[j][k] = aux1[k];
                        }
                    }
                }
            }
        }
        for (int i = 0; i < CONT; i++) {
            System.out.println(ANSI_PURPLE + "Tasca " + (i + 1) + " => '" + aux[i][1] + "' a realitzar en " + aux[i][3] + " minuts [Prioritat " + aux[i][4] + "] (" + aux[i][5] + ")" + ANSI_RESET);
        }
    }

    public static void afegirTemps(String[][] matriz) {
        mostrarTasques(matriz);
        System.out.println();
        System.out.print("Introdueix el número de la tasca: ");
        int afigTemps = teclado.nextInt();
        System.out.print("Quant de temps vols afegir?: ");
        int tempsTotal = teclado.nextInt();

        int numEntero = Integer.parseInt(matriz[afigTemps - 1][3]);
        int total = numEntero + tempsTotal;
        if (total > 1000 || total < 5) {
            System.out.println(ANSI_RED + "El temps no pot superear els límits! [5-1000]" + ANSI_RESET);
            afegirTemps(matriz);
        }

        String totalFinal = String.valueOf(total);
        matriz[afigTemps - 1][3] = totalFinal;
        System.out.println(ANSI_GREEN + "Temps afegit amb èxit" + ANSI_RESET);
    }
}